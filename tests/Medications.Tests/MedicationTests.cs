﻿using Medications.Domain.Entities;
using Medications.Domain.Exceptions;
using Xunit;

namespace Medications.Tests
{
    public class MedicationTests
    {
        [Fact]
        public void Medication_ShouldHaveName()
        {
            //Arrange, Act
            var ex = Assert.Throws<DomainException>(() =>
                new Medication(string.Empty, 1)
            );

            //Assert

            Assert.Equal("The Medication Name is required.", ex.Message);
        }

        [Fact]
        public void Medication_ShouldHaveQuantityGreaterThanZero()
        {
            //Arrange, Act
            var ex = Assert.Throws<DomainException>(() =>
                new Medication("Meds", 0)
            );

            //Assert

            Assert.Equal("The Medication Quantity is required and must be greater than 0", ex.Message);
        }
    }
}
