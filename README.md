# README #

SmartMed - Medications API V1

### Instructions ###

The database used in this API is mysql, you can find a starting script on databases folder, you need to run it in your local database and change the connectionString details on appsettings.Development.json/appsettings.json files

### Points of Improvement ###

* Change table id from int to Guid
* Replace IConfiguration injection in repository to an abstraction
* Implement authorization
* Implement Circuit Breaker strategy using polly
* Implement caching using IMemoryCache
* Segregate query and command responsibilities
* Improve testing coverage