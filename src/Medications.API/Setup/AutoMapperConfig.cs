﻿using System;
using AutoMapper;
using Medications.Application.AutoMapper;
using Microsoft.Extensions.DependencyInjection;

namespace Medications.API.Setup
{
    public static class AutoMapperConfig
    {
        public static void AddAutoMapperConfiguration(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddAutoMapper(typeof(DomainToDtoMappingProfile), typeof(DtoToDomainMappingProfile));
        }
    }
}
