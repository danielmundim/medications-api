﻿using System;
using Medications.Application.Services;
using Medications.Domain.Interfaces;
using Medications.Infrastructure.Data.Repository;
using Microsoft.Extensions.DependencyInjection;

namespace Medications.API.Setup
{
    public static class DependencyInjectionConfig
    {
        public static void AddDependencyInjectionConfiguration(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            // Infra - Data
            services.AddScoped<IMedicationRepository, MedicationRepository>();

            // Application
            services.AddScoped<IMedicationService, MedicationService>();

        }
    }
}
