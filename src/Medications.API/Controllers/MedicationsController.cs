﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Medications.Application.Dto;
using Medications.Application.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Medications.API.Controllers
{
    [ApiController]
    [Route("v1/[controller]")]
    public class MedicationsController : ApiController
    {
        private readonly IMedicationService medicationService;

        public MedicationsController(IMedicationService medicationService)
        {
            this.medicationService = medicationService;
        }

        /// <summary>
        /// Gets all available medications.
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<IEnumerable<MedicationDto>> Get()
        {
            return await this.medicationService.GetAllASync();
        }

        /// <summary>
        /// Creates a medication based on received parameters.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="quantity"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Create(MedicationDto medication)
        {
            return !ModelState.IsValid ? CustomResponse(ModelState) : CustomResponse(await this.medicationService.CreateAsync(medication));
        }

        /// <summary>
        /// Deletes a medication based on the received id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (id <= 0) return NotFound();

            if (!await this.medicationService.DeleteAsync(id)) return NotFound();

            return this.Ok();
        }
    }
}