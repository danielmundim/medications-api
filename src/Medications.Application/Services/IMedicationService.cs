﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Medications.Application.Dto;

namespace Medications.Application.Services
{
    public interface IMedicationService : IDisposable
    {
        Task<IEnumerable<MedicationDto>> GetAllASync();
        Task<bool> CreateAsync(MedicationDto medication);
        Task<bool> DeleteAsync(int id);
    }
}
