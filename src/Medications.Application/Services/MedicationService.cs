﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Medications.Application.Dto;
using Medications.Domain.Entities;
using Medications.Domain.Interfaces;

namespace Medications.Application.Services
{
    public class MedicationService : IMedicationService
    {
        private readonly IMedicationRepository medicationRepository;
        private readonly IMapper mapper;

        public MedicationService(IMedicationRepository medicationRepository, IMapper mapper)
        {
            this.medicationRepository = medicationRepository;
            this.mapper = mapper;
        }

        public async Task<IEnumerable<MedicationDto>> GetAllASync()
        {
            return this.mapper.Map<IEnumerable<MedicationDto>>(await this.medicationRepository.GetAllAsync());
        }

        public async Task<bool> CreateAsync(MedicationDto medication)
        {
            return await this.medicationRepository.CreateAsync(this.mapper.Map<Medication>(medication));
        }

        public async Task<bool> DeleteAsync(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentException("Please insert a valid id.");
            }
            return await this.medicationRepository.DeleteAsync(id);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}