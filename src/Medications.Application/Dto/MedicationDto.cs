﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Medications.Application.Dto
{
    public class MedicationDto
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "The Name is Required")]
        [MaxLength(300)]
        public string Name { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 0")]
        public int Quantity { get; set; }

        public DateTime? Created_On { get; set; }
    }
}
