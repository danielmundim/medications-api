﻿using AutoMapper;
using Medications.Application.Dto;
using Medications.Domain.Entities;

namespace Medications.Application.AutoMapper
{
    public class DtoToDomainMappingProfile : Profile
    {
        public DtoToDomainMappingProfile()
        {
            CreateMap<MedicationDto, Medication>();
        }
    }
}
