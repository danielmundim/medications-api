﻿using AutoMapper;
using Medications.Application.Dto;
using Medications.Domain.Entities;

namespace Medications.Application.AutoMapper
{
    public class DomainToDtoMappingProfile : Profile
    {
        public DomainToDtoMappingProfile()
        {
            CreateMap<Medication, MedicationDto>();
        }
    }
}
