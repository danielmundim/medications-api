﻿using Dapper;
using Medications.Domain.Entities;
using Medications.Domain.Interfaces;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Medications.Infrastructure.Data.Repository
{
    public class MedicationRepository : IMedicationRepository
    {
        private string connectionString { get; }

        public MedicationRepository(IConfiguration config)
        {
            this.connectionString = config.GetSection("ConnectionStrings:DefaultConnection").Value;
        }

        public async Task<IEnumerable<Medication>> GetAllAsync()
        {
            using (var connection = new MySqlConnection(this.connectionString))
            {
                await connection.OpenAsync();

                var query = "select * from medications;";
                var result = await connection.QueryAsync<Medication>(query);

                return result.ToList();
            }
        }

        public async Task<bool> CreateAsync(Medication medication)
        {
            using (var connection = new MySqlConnection(this.connectionString))
            {
                await connection.OpenAsync();

                var query = "insert into medications (name, quantity, created_on) values (@name, @quantity, @created_on);";
                return await connection.ExecuteAsync(query, medication) > 0;
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            using (var connection = new MySqlConnection(this.connectionString))
            {
                await connection.OpenAsync();

                var query = "delete from medications where id = @id;";
                return await connection.ExecuteAsync(query, new { id }) > 0;
            }
        }
    }
}
