﻿using Medications.Domain.Interfaces;

namespace Medications.Domain.Entities
{
    public abstract class Entity : IEntity
    {
        public int Id { get; set; }
    }
}
