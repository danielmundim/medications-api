﻿using System;
using Medications.Domain.Validations;

namespace Medications.Domain.Entities
{
    public class Medication : Entity
    {
        public Medication(string name, int quantity)
        {
            GeneralValidations.ValidateIfNullOrEmpty(name, "The Medication Name is required.");
            GeneralValidations.ValidateIfValueIsLessThan(quantity, 1, "The Medication Quantity is required and must be greater than 0");

            this.Name = name;
            this.Quantity = quantity;
            this.Created_On = DateTime.Now;
        }

        public Medication() { }

        public string Name { get; private set; }
        public int Quantity { get; private set; }
        public DateTime Created_On { get; private set; }
    }
}
