﻿namespace Medications.Domain.Interfaces
{
    public interface IEntity
    {
        public int Id { get; }
    }
}
