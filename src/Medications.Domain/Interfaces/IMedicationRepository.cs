﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Medications.Domain.Entities;

namespace Medications.Domain.Interfaces
{
    public interface IMedicationRepository
    {
        Task<IEnumerable<Medication>> GetAllAsync();
        Task<bool> CreateAsync(Medication medication);
        Task<bool> DeleteAsync(int id);
    }
}
