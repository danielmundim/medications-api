﻿using Medications.Domain.Exceptions;

namespace Medications.Domain.Validations
{
    public static class GeneralValidations
    {
        public static void ValidateIfValueIsLessThan(long value, long minValue, string message)
        {
            if (value < minValue)
            {
                throw new DomainException(message);
            }
        }

        public static void ValidateIfNullOrEmpty(string value, string message)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new DomainException(message);
            }
        }
    }
}
