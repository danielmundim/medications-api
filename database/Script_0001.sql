create table medications (
	id int not null primary key auto_increment,
    name varchar(300) not null,
    quantity int not null,
    created_on datetime not null
);

insert into medications (name, quantity, created_on) values ('Synthroid', 5, now());
insert into medications (name, quantity, created_on) values ('Crestor', 10, now());
insert into medications (name, quantity, created_on) values ('Ventolin', 15, now());
insert into medications (name, quantity, created_on) values ('Nexium', 20, now());
insert into medications (name, quantity, created_on) values ('Advair Diskus', 25, now());
insert into medications (name, quantity, created_on) values ('Lantus Solostar', 30, now());
insert into medications (name, quantity, created_on) values ('Vyvanse', 35, now());
insert into medications (name, quantity, created_on) values ('Lyrica', 40, now());
insert into medications (name, quantity, created_on) values ('Spiriva', 45, now());
insert into medications (name, quantity, created_on) values ('Januvia', 50, now());
